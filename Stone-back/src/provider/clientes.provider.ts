import { Connection, Repository } from 'typeorm';
import { Pessoa } from './../entities/pessoa.entity';

export const ClientesProvider = [
  {
    provide: 'ClienteRepositoryToken',
    useFactory: (connection: Connection) => connection.getRepository(Pessoa),
    inject: ['DbConnectionToken'],
  },
];