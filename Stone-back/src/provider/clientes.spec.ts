import { Test, TestingModule } from '@nestjs/testing';
import { Clientes } from './clientes';

describe('Clientes', () => {
  let provider: Clientes;
  
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Clientes],
    }).compile();
    provider = module.get<Clientes>(Clientes);
  });
  it('should be defined', () => {
    expect(provider).toBeDefined();
  });
});
