import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Pessoa } from './../../entities/pessoa.entity';
import { Contas } from './../../entities/conta.entity';

@Injectable()
export class ContasService {
  constructor(
    @InjectRepository(Contas)
    private readonly ContasRepository: Repository<Contas>,
  ) { }

  async findAll(): Promise<Contas[]> {
    return await this.ContasRepository.find();
  }

  async add(o: any): Promise<Contas> {
    const c = new Contas();
    c.dtMovimento = new Date();
    c.numero = o.numero;
    c.pessoa_idpessoa = o.pessoa;
    c.saldo = 1000;
    await c.save();
    return c;
  }
}