import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Movimento } from './../../entities/movimento.entity';

@Injectable()
export class MovimentosService {
  constructor(
    @InjectRepository(Movimento)
    private readonly MovimentosRepository: Repository<Movimento>,
  ) { }

  async findAll(): Promise<Movimento[]> {
    return await this.MovimentosRepository.find();
  }

  async add(o: any): Promise<Movimento> {
    const m = new Movimento();
    m.contas_idcontas = o.conta;
    m.credito = o.credito;
    m.data = new Date();
    m.debito = o.debito;
    m.historico = o.historico;
    await m.save();
    return m;
  }
}