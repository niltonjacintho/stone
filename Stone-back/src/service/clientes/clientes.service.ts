import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Pessoa } from './../../entities/pessoa.entity';
import { Movimento } from '../../entities/movimento.entity';
import { Contas } from '../../entities/conta.entity';

import { getManager, getConnection, TransactionManager } from 'typeorm';


@Injectable()
export class ClienteService {
  constructor(
    @InjectRepository(Pessoa)
    private readonly ClienteRepository: Repository<Pessoa>,
  ) { }

  async findAll(): Promise<Pessoa[]> {
    return await this.ClienteRepository.find();
  }

  async findByCpf(token: string): Promise<Pessoa> {
    console.log('TOKEN   ' + token, this.ClienteRepository.find({ where: { cpf: token } }));
    this.ClienteRepository.find({ where: { cpf: token } })
    return await this.ClienteRepository.find({ where: { cpf: token } })
      .then((data: Pessoa[]) => {
        const p: Pessoa = data[0];
        return p;
      });
  }

  async add(o: any): Promise<Pessoa> {
    // console.log(o);
    const p = await this.savePessoa(o);
    console.log('objeto pessoa', p);
    const c = await this.saveConta(p);
    //  const m = await this.saveMovimentoInicial(c);
    console.log('p', p);
    return new Pessoa();
  }

  async savePessoa(pessoa: any): Promise<Pessoa> {

    const p = new Pessoa();

    p.cpf = pessoa.cpf;
    p.endereco = pessoa.endereco;
    p.identidade = pessoa.identidade;
    p.nome = pessoa.nome;
    p.sobrenome = pessoa.sobrenome;
    p.telefone = pessoa.telefone;
    await p.save();
    console.log('p', p.idpessoa)
    return p;
  }

  async saveConta(p: Pessoa): Promise<Contas> {
    console.log('p', p);
    const c = new Contas();
    c.numero = p.idpessoa;
    c.pessoa_idpessoa = p.idpessoa;
    c.saldo = 1000;
    c.dtMovimento = new Date();
    console.log('c before save', c)
    await c.save();
    return c;
  }

  async saveMovimentoInicial(c: Contas): Promise<Movimento> {
    const m = new Movimento();
    m.data = new Date();
    m.debito = 0;
    m.historico = 'Desposito Inicial';
    m.credito = 1000;
    m.contas_idcontas = c.idcontas;
    console.log(m);
    await m.save();
    return m;
  }
}