import { Injectable, Logger } from '@nestjs/common';
import { ClienteService } from './../clientes/clientes.service';

@Injectable()
export class AuthService {
    constructor(public clienteService: ClienteService) {
        console.log('teste');
    }

    async validateUser(token: string): Promise<any> {
        console.log('inside validate');
        // Validate if token passed along with HTTP request
        // is associated with any registered account in the database
        // this.clienteSrv.findByCpf('11');
        return await this.clienteService.findByCpf(token);
    }
}