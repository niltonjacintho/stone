import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { ClienteModule } from './module/cliente/cliente.module';
import { DatabaseModule } from './module/database/database.module';
import { MovimentoModule } from './module/movimento/movimento.module';
import { ContaModule } from './module/conta/conta.module';
import { ContasService } from './service/contas/contas.service';
import { MovimentosService } from './service/movimentos/movimentos.service';
import { AuthService } from './service/auth/auth.service';
import { AuthModule } from './module/auth/auth.module';
import { ClienteService } from './service/clientes/clientes.service';


@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '127.0.0.1',
      port: 3306,
      username: 'root',
      password: 'stone',
      database: 'mydb',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: false,
    }),
    // TypeOrmModule.forRoot(),
    DatabaseModule,
    ClienteModule,
    MovimentoModule,
    ContaModule,
    AuthModule,

  ],
  controllers: [AppController],
  providers: [AppService, ContasService, MovimentosService, AuthService, ClienteService],
})
export class AppModule {
  constructor(private readonly connection: Connection) { }
}
