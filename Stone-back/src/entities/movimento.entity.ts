import { Entity, Column, PrimaryGeneratedColumn, Double, BeforeInsert, BaseEntity } from 'typeorm';
import * as uuidv4 from 'uuid/v4';

@Entity()
export class Movimento extends BaseEntity {
    @PrimaryGeneratedColumn()
    idmovimento: string;

    @Column()
    data: Date;

    @Column()
    credito: number;

    @Column()
    debito: number;

    @Column({ length: 450 })
    historico: string;

    @Column()
    contas_idcontas: string;

    @BeforeInsert()
    addId() {
        this.idmovimento = uuidv4();
    }
}
