import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, BaseEntity } from 'typeorm';
import * as uuidv4 from 'uuid/v4';

@Entity()
export class Pessoa extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    idpessoa: string;

    @Column({ length: 100 })
    nome: string;

    @Column({ length: 150 })
    sobrenome: string;

    @Column()
    cpf: string;

    @Column()
    identidade: string;

    @Column()
    endereco: string;

    @Column()
    telefone: string;

    @BeforeInsert()
    addId() {
        this.idpessoa = uuidv4();
    }

}
