import { Entity, Column, OneToMany, JoinColumn, PrimaryGeneratedColumn, Double, BeforeInsert, BaseEntity } from 'typeorm';
import { Pessoa } from './pessoa.entity';
import * as uuidv4 from 'uuid/v4';

@Entity()
export class Contas extends BaseEntity {
    @PrimaryGeneratedColumn()
    idcontas: string;

    @Column()
    dtMovimento: Date;

    @Column()
    saldo: number;

    @Column({ length: 20 })
    numero: string;

    @OneToMany(type => Pessoa, pessoa => pessoa.idpessoa)
    //  @JoinColumn({ name: 'pessoa_idpessoa' })
    pessoa_idpessoa: string;

    @BeforeInsert()
    addId() {
        this.idcontas = uuidv4();
    }

}
