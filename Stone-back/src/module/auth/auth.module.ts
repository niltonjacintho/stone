import { Module } from '@nestjs/common';
import { AuthService } from './../../service/auth/auth.service';
import { HttpStrategy } from './../../service/auth/http-strategy';
import { ClienteModule } from './../cliente/cliente.module';
import { ClienteService } from '../../service/clientes/clientes.service';

@Module({
    imports: [ClienteModule],
    providers: [AuthService, HttpStrategy, ClienteService],
})
export class AuthModule { }