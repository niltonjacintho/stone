import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MovimentosService } from './../../service/movimentos/movimentos.service';
import { MovimentosController } from './../../controllers/movimentos/movimentos.controller';
import { Movimento } from './../../entities/movimento.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Movimento])],
  providers: [MovimentosService],
  controllers: [MovimentosController],
})
export class MovimentoModule { }