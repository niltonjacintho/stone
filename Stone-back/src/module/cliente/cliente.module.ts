import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClienteService } from './../../service/clientes/clientes.service';
import { ClienteController } from './../../controllers/clientes/clientes.controller';
import { Pessoa } from './../../entities/pessoa.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Pessoa])],
  providers: [ClienteService],
  controllers: [ClienteController],
})
export class ClienteModule { }