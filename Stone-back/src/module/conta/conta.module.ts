
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ContasService } from './../../service/contas/contas.service';
import { ContasController } from './../../controllers/Contas/contas.controller';
import { Contas } from './../../entities/conta.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Contas])],
  providers: [ContasService],
  controllers: [ContasController],
})
export class ContaModule { }