import { Controller, Get, Post, Body, UseGuards } from '@nestjs/common';
import { ClienteService } from './../../service/clientes/clientes.service';
import { AuthGuard } from '@nestjs/passport';
import { Pessoa } from './../../entities/pessoa.entity';
import { Transaction } from 'typeorm';

@Controller('cliente')
export class ClienteController {
  constructor(private readonly clienteService: ClienteService) { }

  @Get()
  @UseGuards(AuthGuard('bearer'))
  findAll(): Promise<Pessoa[]> {
    return this.clienteService.findAll();
  }

  @Post('add')
  // @Transaction()
  add(@Body() body): Promise<Pessoa> {
    return this.clienteService.add(body);
  }
}