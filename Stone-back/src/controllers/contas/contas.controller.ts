
import { Controller, Get, Post, Body } from '@nestjs/common';
import { ContasService } from './../../service/contas/contas.service';
import { Contas } from './../../entities/conta.entity';

@Controller('contas')
export class ContasController {
  constructor(private readonly contaSrv: ContasService) { }

  @Get()
  findAll(): Promise<Contas[]> {
    return this.contaSrv.findAll();
  }

  @Post('add')
  add(@Body() body): Promise<Contas> {
    return this.contaSrv.add(body);
  }
}