import { Test, TestingModule } from '@nestjs/testing';
import { ContasController } from './contas.controller';

describe('Contas Controller', () => {
  let module: TestingModule;
  
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [ContasController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: ContasController = module.get<ContasController>(ContasController);
    expect(controller).toBeDefined();
  });
});
