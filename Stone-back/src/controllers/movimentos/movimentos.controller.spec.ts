import { Test, TestingModule } from '@nestjs/testing';
import { MovimentosController } from './movimentos.controller';

describe('Movimentos Controller', () => {
  let module: TestingModule;
  
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [MovimentosController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: MovimentosController = module.get<MovimentosController>(MovimentosController);
    expect(controller).toBeDefined();
  });
});
