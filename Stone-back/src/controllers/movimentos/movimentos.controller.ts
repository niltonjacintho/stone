import { Controller, Get, Post, Body } from '@nestjs/common';
import { MovimentosService } from './../../service/movimentos/movimentos.service';
import { Movimento } from './../../entities/movimento.entity';

@Controller('movimentos')
export class MovimentosController {
  constructor(private readonly movimentoSrv: MovimentosService) { }

  @Get()
  findAll(): Promise<Movimento[]> {
    return this.movimentoSrv.findAll();
  }

  @Post('add')
  add(@Body() body): Promise<Movimento> {
    return this.movimentoSrv.add(body);
  }
}