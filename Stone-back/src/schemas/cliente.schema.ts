import * as mongoose from 'mongoose';

export const ClienteSchema = new mongoose.Schema({
  id: Number,
  usuario: String,
  nome: String,
  sobrenome: String,
  cpf: String,
  identidade: String,
  telefone: String,
  email: String,
  idconta: Number,
});